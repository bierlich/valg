# coding=utf-8
import xml.etree.ElementTree as et 
import urllib 
import os
class Kreds:
    pass

def getroot(url): 
    name = url.split('/')
    elecname = name[-3]
    filename = name[-1]
    if os.path.isdir(elecname):
        try: 
            f = open(elecname + "/" + filename)
            f.close() 
        except: 
            urllib.urlretrieve(url,elecname + "/" + filename) 
    else: 
        os.mkdir(elecname) 
        urllib.urlretrieve(url,elecname + "/" + filename) 
    f = open(elecname + "/" + filename) 
    tree = et.parse(f) 
    f.close() 
    return tree.getroot()

# Stemmer afgivet i landsdele
def landsniveau(root):
    kredse = []
    valgnavn = ""
    for child in root:
        if child.tag == "ValgNavn":
            valgnavn = child.text
        if child.tag == "Landsdele":
            for sub in child:
                k = Kreds()
                k.name = sub.text.strip('1234567890.').strip()
                kredsroot = getroot(sub.get("filnavn"))
                for ch in kredsroot: 
                    if ch.tag == "Status":
                        k.status = int(ch.get("Kode"))
                    elif ch.tag == "Stemmeberettigede":
                        k.voters = int(ch.text)
                    elif ch.tag == "Stemmer":
                        for party in ch:
                            if party.get("Bogstav") is not None:
                                let = party.get("Bogstav")
                                if let == "Ø":
                                    let = "OE"
                                if let == "Å":
                                    let = "AA"
                                if let == "":
                                    let = "U"
                                setattr(k, let + "votes",party.get("StemmerAntal"))
                                setattr(k, let + "pct",party.get("StemmerPct"))
                                setattr(k, let + "seats",party.get("Mandater"))
                            elif party.tag == "IAltGyldigeStemmer":
                                k.validvotes = int(party.text)
                            elif party.tag == "IAltUgyldigeStemmer":
                                k.invalidvotes = int(party.text)
                            elif party.tag == "BlankeStemmer":
                                k.blankvotes = int(party.text)
                kredse.append(k)
    return kredse, valgnavn

# Stemmer afgivet i opstillingskredse
def kredsniveau(root,plist=[], pshort=[]):
    kredse = []
    valgnavn = ""
    for child in root:
        if child.tag == "ValgNavn":
            valgnavn = child.text
        if child.tag == "Opstillingskredse":
            for sub in child:
                k = Kreds()
                k.name = sub.text.strip('1234567890.').strip()
                k.opstkreds = sub.get("opstillingskreds_id")
                kredsroot = getroot(sub.get("filnavn"))
                for ch in kredsroot: 
                    if ch.tag == "Status":
                        k.status = int(ch.get("Kode"))
                    elif ch.tag == "Stemmeberettigede":
                        k.voters = int(ch.text)
                    elif ch.tag == "Stemmer":
                        for party in ch:
                            if party.get("Bogstav") is not None:
                                let = party.get("Bogstav")
                                if let == "Ø":
                                    let = "OE"
                                if let == "Å":
                                    let = "AA"
                                setattr(k, let + "votes",party.get("StemmerAntal"))
                            elif party.tag == "IAltGyldigeStemmer":
                                k.validvotes = int(party.text)
                            elif party.tag == "IAltUgyldigeStemmer":
                                k.invalidvotes = int(party.text)
                    elif ch.tag == "Personer":
                        for party in ch:
                            for person in party:
                                for p,q in zip(plist,pshort):
                                    if person.get("Navn") == p:
                                        setattr(k,q,int(person.get("PersonligeStemmer"))) 
                kredse.append(k)
    return kredse, valgnavn

# Stemmer afgivet per område
def omrniveau(root,knumber=[],plist=[], pshort=[], allCandidates=False):
    kredse = []
    allkeys = []
    valgnavn = ""
    for child in root:
        if child.tag == "ValgNavn":
            valgnavn = child.text
        if child.tag == "Afstemningsomraader":
            for sub in child:
                k = Kreds()
                k.name = sub.text
                #sub.text.strip('1234567890.').strip()
                k.opstkreds = sub.get("opstillingskreds_id")
                if len(knumber) > 0 and k.opstkreds not in knumber:
                    continue
                kredsroot = getroot(sub.get("filnavn"))
                for ch in kredsroot: 
                    if ch.tag == "Status":
                        k.status = int(ch.get("Kode"))
                    elif ch.tag == "Stemmeberettigede":
                        k.voters = int(ch.text)
                    elif ch.tag == "Stemmer":
                        for party in ch:
                            if party.get("Bogstav") is not None:
                                let = party.get("Bogstav")
                                if let == "Ø":
                                    let = "OE"
                                if let == "Å":
                                    let = "AA"
                                setattr(k, let + "votes",party.get("StemmerAntal"))
                            elif party.tag == "IAltGyldigeStemmer":
                                k.validvotes = int(party.text)
                            elif party.tag == "IAltUgyldigeStemmer":
                                k.invalidvotes = int(party.text)
                    elif ch.tag == "Personer":
                        for party in ch:
                            for person in party:
                                if allCandidates:
                                    key = person.get("Navn").replace(" ","")
                                    allkeys.append(key)
                                    setattr(k,key,int(person.get("PersonligeStemmer")))
                                else: 
                                    for p,q in zip(plist,pshort):
                                        if person.get("Navn") == p:
                                            setattr(k,q,int(person.get("PersonligeStemmer")))
                kredse.append(k)
    if allCandidates:
        return kredse, valgnavn, list(dict.fromkeys(allkeys))
    return kredse, valgnavn


import sys
reload(sys)
sys.setdefaultencoding('utf8')

ep09 = "http://www.dst.dk/valg/Valg1191212/xml/fintal.xml"
ep14 = "http://www.dst.dk/valg/Valg1475795/xml/fintal.xml"
ep19 = "http://www.dst.dk/valg/Valg1684426/xml/fintal.xml"

ft11 = "http://www.dst.dk/valg/Valg1204271/xml/fintal.xml"
ft15 = "http://www.dst.dk/valg/Valg1487635/xml/fintal.xml"
ft19 = "http://www.dst.dk/valg/Valg1684447/xml/fintal.xml"

