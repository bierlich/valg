# coding=utf-8
from valg import *
from annotefinder import *
import numpy 
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import geopandas as gpd
from descartes import PolygonPatch

import re
import sys
reload(sys)
sys.setdefaultencoding('utf8')

cmap = plt.cm.tab20

# Find all københavnerkredse
names = ["Jonathan Simmel"]
snames = ["js"]
purge = ["10. Kreds, Søerne","Tårnby","Korsvejen","Løjtegården","Kastrup"]
kredse, navn = omrniveau(getroot(ft19),[],names,snames)

kbh = []
for k in kredse:
    if hasattr(k,"js") and k.name not in purge:
        kbh.append(k.opstkreds)
kbh = list(dict.fromkeys(kbh))

kredse, navn, allcands = omrniveau(getroot(ft19),kbh,[],[],True)

mnames = []
for k in kredse:
    if k.name in purge:
        continue
    maxvotes = 0
    maxname = ""
    for sn in allcands:
        try:
            if getattr(k,sn) > maxvotes:
                maxvotes = getattr(k,sn)
                maxname = sn
        except:
            print sn
            print k.name
    mnames.append(maxname)
mnames = list(dict.fromkeys(mnames))
snames = []
deltacol = 1./float(len(mnames))
col = 0.2
cols = []
for n in mnames:
    cols.append(cmap(col))
    col+=deltacol
    snames.append(n.replace(" ",""))

plt.rcParams['figure.figsize'] = (20, 10)
cphmap = gpd.read_file('kbh.geojson')
ax = cphmap.plot()

mnames2 = []
for k in kredse:
    if k.name in purge:
        continue
    maxvotes = 0
    maxname = ""
    for sn in snames:
        try:
            if getattr(k,sn) > maxvotes:
                maxvotes = getattr(k,sn)
                maxname = sn
        except:
            print sn
            print k.name
    if maxname == "IdaAuken" or maxname == "PeterHummelgaard":
        print k.name
    for idx, row in cphmap.iterrows():
        if row["navn"] == k.name:
            mnames2.append(maxname)
            ax.add_patch(PolygonPatch(row["geometry"],fc=cols[snames.index(maxname)]))

plt.tick_params(axis='both',which='both',bottom=False,top=False,left=False,right=False,labelbottom=False,labelleft=False)

leg_lines = []
leg_names = []
for c,n,sn in zip(cols,mnames,snames):
    if sn in mnames:
        leg_lines.append(Line2D([0],[0],color=c,lw=4))
        leg_names.append(re.sub(r"(\w)([A-Z])", r"\1 \2", n))
ax.legend(leg_lines, leg_names,loc=4,prop={'size':20})

plt.savefig("kbh-top.png")

plt.show() 

