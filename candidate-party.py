# coding=utf-8
from valg import *
from annotefinder import *
import numpy 
import matplotlib.pyplot as plt

import sys
reload(sys)
sys.setdefaultencoding('utf8')

#names = ["Jonathan Simmel","Pernille Skipper", "Rosa Lund", "Rune Lund", "Jette Gottlieb", "Louis Jacobsen","Michael Neumann",]
#snames = ["js","ps","rol","rul","jg","lj","mn"]

names = ["Jonathan Simmel", "Jette Gottlieb", "Louis Jacobsen","Michael Neumann",]
snames = ["js","jg","lj","mn"]

yvals = []
for n in names:
    yvals.append([])

labels = []

kredse, navn = omrniveau(getroot(ft19),-1,names,snames)

i = 0
x = []
y = []

num = 0

for k in kredse:
    if hasattr(k,"js"):
        j = 0
        for sn in snames:
            yvals[j].append(getattr(k,sn))
            if sn == "js":
                num+=getattr(k,sn)
            j+=1
        x.append(i)
        i+=1
        labels.append(k.name)

fig, ax = plt.subplots()
for yy,cand in zip(yvals,names):
    ax.scatter(x,yy,label=cand)
    af = AnnoteFinder(x, yy, labels, ax=ax, xtol=0.5,ytol=5)
    fig.canvas.mpl_connect('button_press_event', af)
ax.legend()

plt.show() 

#x = []
#y = []
#label = []
#for k1,k2 in zip(kredse1,kredse2):
#    try: 
#        xval = float(k1.Cvotes)/float(k1.validvotes)
#        yval = float(k2.Cvotes)/float(k2.validvotes)
#        x.append(xval)
#        y.append(yval)
#        label.append(k1.name)
#    except: print "Problem at ",k1.name
#import numpy 
#import matplotlib.pyplot as plt

#fig, ax = plt.subplots()

#ax.scatter(x, y)
#af = AnnoteFinder(x, y, label, ax=ax, xtol=0.05,ytol=0.05)
#fig.canvas.mpl_connect('button_press_event', af)

#plt.xlabel(navn1)
#plt.ylabel(navn2)
#plt.show() 

