# coding=utf-8
from valg import *
from annotefinder import *
import numpy 
import matplotlib.pyplot as plt

import sys
reload(sys)
sys.setdefaultencoding('utf8')

# find køge
kredse1, _ = kredsniveau(getroot(ep19))
knumber = -1
for k in kredse1:
    if k.name == "Køge":
        knumber = k.opstkreds

kredse1, navn1 = omrniveau(getroot(ep19),[knumber])
kredse2, navn2 = omrniveau(getroot(ft19),[knumber])

x = []
y = []
label = []
for k1,k2 in zip(kredse1,kredse2):
    try: 
        xval = float(k1.Fvotes)/float(k1.validvotes)
        yval = float(k2.Fvotes)/float(k2.validvotes)
        x.append(xval)
        y.append(yval)
        label.append(k1.name)
    except: print "Problem at ",k1.name
import numpy 
import matplotlib.pyplot as plt

fig, ax = plt.subplots()

ax.scatter(x, y)
af = AnnoteFinder(x, y, label, ax=ax, xtol=0.05,ytol=0.05)
fig.canvas.mpl_connect('button_press_event', af)

plt.xlabel(navn1)
plt.ylabel(navn2)
plt.show() 

