import geopandas as gpd
import matplotlib.pyplot as plt
from descartes import PolygonPatch

plt.rcParams['figure.figsize'] = (20, 10)

cphmap = gpd.read_file('kbh.geojson')

ts = []
for idx, row in cphmap.iterrows():
    if row["navn"] == "11. Kreds, Lindevang":
        print "ping"
        ts = row["geometry"]

ax = cphmap.plot()
ax.add_patch(PolygonPatch(ts,fc='red'))
plt.show()
