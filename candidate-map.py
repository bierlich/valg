# coding=utf-8
from valg import *
from annotefinder import *
import numpy 
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import geopandas as gpd
from descartes import PolygonPatch

import sys
reload(sys)
sys.setdefaultencoding('utf8')

cmap = plt.cm.tab20

names = ["Jonathan Simmel", "Jette Gottlieb", "Louis Jacobsen","Michael Neumann","Dorthe Hecht","David Rønne","Jakob Nerup","Karen Skærlund Risvig","Mette Bang Larsen"]
snames = ["js","jg","lj","mn","dh","dr","jn","kr","ml"]
cols = [cmap(0.),cmap(0.1),cmap(0.2),cmap(0.3),cmap(0.4),cmap(0.5),cmap(0.6),cmap(0.7),cmap(0.8)]

kredse, navn = omrniveau(getroot(ft19),[],names,snames)

plt.rcParams['figure.figsize'] = (20, 10)
cphmap = gpd.read_file('kbh.geojson')
ax = cphmap.plot()

mnames = []
for k in kredse:
    if not hasattr(k,"js"):
        continue
    maxvotes = 0
    maxname = ""
    for sn in snames:
        try:
            if getattr(k,sn) > maxvotes:
                maxvotes = getattr(k,sn)
                maxname = sn
        except:
            print sn
            print k.name
    for idx, row in cphmap.iterrows():
        if row["navn"] == k.name:
            mnames.append(maxname)
            ax.add_patch(PolygonPatch(row["geometry"],fc=cols[snames.index(maxname)]))

plt.tick_params(axis='both',which='both',bottom=False,top=False,left=False,right=False,labelbottom=False,labelleft=False)

leg_lines = []
leg_names = []
for c,n,sn in zip(cols,names,snames):
    if sn in mnames:
        leg_lines.append(Line2D([0],[0],color=c,lw=4))
        leg_names.append(n)
ax.legend(leg_lines, leg_names,loc=4,prop={'size':20})

plt.savefig("cph.png")
plt.show() 

