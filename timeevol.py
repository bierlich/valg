# coding=utf-8
from valg import *
from annotefinder import *
import numpy 
import matplotlib.pyplot as plt

import sys
reload(sys)
sys.setdefaultencoding('utf8')

year = [2011, 2015, 2019]
ft = [ft11, ft15, ft19]
P = ["A", "B", "C", "D", "F", "I", "O", "P", "V", "OE", "AA"]

# find landsdele
part = []
k, _ = landsniveau(getroot(ft19))
for ch in k:
    part.append(ch.name)

figs = []
axs = []
for ld in part:
    fig, ax = plt.subplots() 
    for p in P:
        x = []
        y = []
        for annum, f in zip(year,ft):
            k, _ = landsniveau(getroot(f))
            for ch in k:
                if ch.name == ld:
                    if hasattr(ch, p+"pct"):
                        x.append(annum)
                        y.append(getattr(ch, p+"pct"))
                    else:
                        x.append(annum)
                        y.append(0.)
        ax.plot(x,y,label=p)
        ax.legend()
        ax.set_title(ld)
        ax.set_ylabel("Stemmer pct.")
        ax.set_xlabel("Årstal")
        #fig.savefig(ld+".pdf")

plt.show()

 

#fig, ax = plt.subplots()

#ax.scatter(x, y)
#af = AnnoteFinder(x, y, label, ax=ax, xtol=0.05,ytol=0.05)
#fig.canvas.mpl_connect('button_press_event', af)

#plt.xlabel(navn1)
#plt.ylabel(navn2)
#plt.show() 

